/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mmalloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 16:14:41 by ftriquet          #+#    #+#             */
/*   Updated: 2016/01/20 16:15:09 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mmalloc.h"
#include <stdlib.h>

t_alloc		*new_alloc(size_t size, t_alloc *next)
{
	t_alloc		*res;

	if (!(res = (t_alloc *)malloc(sizeof(t_alloc))))
		return (NULL);
	res->adr = malloc(size);
	res->next = next;
	return (res);
}

void		*bigboss(size_t size, void *addr, int all)
{
	static t_alloc		*allocs = NULL;
	t_alloc				*tmp;

	if (all)
	{
		del_all(&allocs);
		return (NULL);
	}
	else if (!addr)
	{
		allocs = new_alloc(size, allocs);
		return (allocs->adr);
	}
	else if (allocs->adr == addr)
	{
		tmp = allocs;
		tmp = allocs->next;
	}
	else
		remove_alloc(&allocs, addr);
	return (allocs->adr);
}

void		*ft_alloc(size_t size)
{
	return (bigboss(size, NULL, 0));
}

void		ft_free(void *addr)
{
	bigboss(0, addr, 0);
}

void		ft_free_all(void)
{
	bigboss(0, 0, 1);
}
