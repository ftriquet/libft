/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mmalloc.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 16:14:45 by ftriquet          #+#    #+#             */
/*   Updated: 2016/01/20 16:33:48 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MMALLOC_H
# define MMALLOC_H
# include <string.h>

typedef struct		s_alloc
{
	void			*adr;
	struct s_alloc	*next;
}					t_alloc;

void				ft_free(void *addr);
void				ft_free_all(void);
void				*ft_alloc(size_t size);
void				*big_boss(size_t size, void	*addrc);
void				remove_alloc(t_alloc **list, void *addr);
t_alloc				*new_alloc(size_t size, t_alloc *next);
void				del_all(t_alloc **allocs);

#endif
