/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mfree.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 16:14:30 by ftriquet          #+#    #+#             */
/*   Updated: 2016/01/20 16:33:31 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mmalloc.h"
#include <stdlib.h>

void		remove_alloc(t_alloc **list, void *addr)
{
	t_alloc		*tmp;
	t_alloc		*tmp2;

	tmp = *list;
	tmp2 = NULL;
	while (tmp && tmp->adr != addr)
	{
		tmp2 = tmp;
		tmp = tmp->next;
	}
	if (tmp)
	{
		free(tmp->adr);
		if (tmp2)
			tmp2->next = tmp->next;
		else
			*list = tmp->next;
		free(tmp);
	}
}

void		del_all(t_alloc **allocs)
{
	while (*allocs)
		remove_alloc(allocs, (*allocs)->adr);
}
