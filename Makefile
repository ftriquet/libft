# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/28 08:35:37 by ftriquet          #+#    #+#              #
#    Updated: 2016/02/02 13:07:56 by ftriquet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC=ft_atoi.c \
ft_empty_string.c \
strmatch.c \
mmalloc.c \
mfree.c \
init.c \
ft_bstree_add.c \
get_next_line.c \
myprintf.c \
ft_trim.c \
ft_strsubptr.c \
ft_match.c \
ft_realloc.c \
ft_bstree_contains.c \
ft_bstree_del.c \
ft_bstree_iter.c \
ft_bstree_new.c \
ft_bzero.c \
ft_get_index.c \
ft_isalnum.c \
ft_isalpha.c \
ft_isascii.c \
ft_isblank.c \
ft_isdigit.c \
ft_islower.c \
ft_isprint.c \
ft_isupper.c \
ft_itoa.c \
ft_freetab.c \
ft_lstadd.c \
ft_lstadd_back.c \
ft_lstaddcmp.c \
ft_lstdel.c \
ft_lstdel_rec.c \
ft_lstdelone.c \
ft_lstdelcontent.c \
ft_lstgeti.c \
ft_lstget.c \
ft_lstiter.c \
ft_lstmap.c \
ft_lstnew.c \
ft_lstrmdel.c \
ft_lstsize.c \
ft_lstcontains.c \
ft_lstremove.c \
ft_lstremovei.c \
ft_memalloc.c \
ft_memccpy.c \
ft_memchr.c \
ft_memcmp.c \
ft_memcpy.c \
ft_memdel.c \
ft_memmove.c \
ft_memset.c \
ft_putchar.c \
ft_putchar_fd.c \
ft_putendl.c \
ft_putendl_fd.c \
ft_putnbr.c \
ft_putnbr_fd.c \
ft_putstr.c \
ft_putstr_fd.c \
ft_strcat.c \
ft_strchr.c \
ft_strclr.c \
ft_strcmp.c \
ft_strcpy.c \
ft_strdel.c \
ft_strdup.c \
ft_strequ.c \
ft_striter.c \
ft_striteri.c \
ft_strjoin.c \
ft_strlcat.c \
ft_strlen.c \
ft_strmap.c \
ft_strmapi.c \
ft_strncat.c \
ft_strncmp.c \
ft_strncpy.c \
ft_strnequ.c \
ft_strnew.c \
ft_strnlen.c \
ft_strnstr.c \
ft_strrchr.c \
ft_strsplit.c \
ft_copy_word.c \
ft_strstr.c \
ft_strsub.c \
ft_strtrim.c \
ft_tolower.c \
ft_toupper.c

OBJ=$(SRC:.c=.o)

NAME=libft.a

FLAGS=-Wall -Wextra -Werror

all:$(NAME)

$(NAME):$(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)
%.o:%.c
	gcc $(FLAGS) -c $^ -o $@
clean:
	rm -f $(OBJ)
fclean:clean
	rm -f $(NAME)
re:fclean all
